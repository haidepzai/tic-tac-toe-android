# TicTacToe for Android

## Introduction

> A classic Tic Tac Toe game in Java for Android OS

## Detailed Function

There will be a login screen where you can enter your name. There is also a password which is needed to enter (Password: 12345).
After successful confirmination the game activity will open.
Two players X and O place their marks in turns in a 3x3 grid field. The first player who as able to set 3 marks in horizontal, vertical or diagonal row, wins.

## Screenshot

| <a href="https://gitlab.com/haidepzai/tic-tac-toe-android" target="_blank">**Tic Tac Toe Android**</a>


| [![Hai](https://i.ibb.co/rHXDsGm/screenshot.jpg)](https://gitlab.com/haidepzai/tic-tac-toe-android)