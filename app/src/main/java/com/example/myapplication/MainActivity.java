package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    public static String name;
    private Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //res -> layout -> activity_main (Layout)

        button = (Button) findViewById(R.id.submitBTN);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity2();
            }
        });
    }

    public void openActivity2(){
        EditText username = findViewById(R.id.usernameField);
        name = username.getText().toString();
        EditText password = findViewById(R.id.passwordField);
        TextView notification = findViewById(R.id.notificationField);

        if(password.getText().toString().equals("12345")){
            //Open second Activity
            Intent intent = new Intent(this, ActivityOne.class);
            startActivity(intent);
        } else {
            notification.setText("Falsches Passwort!");
        }
    }

}
